
var phone = getvl('phone');
var userName = getvl('userName');
var id = getvl("id")
var orderId = getvl("orderId");

baseMsg(id);

//基础信息
function idCardMsg() {
	$.ajax({
		url: urlcore + "/api/userIdentity/selectOneDetailsByUserId?id=" + id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var da = data.data;
				var status = '';
				if(da.status == 0) {
					status = "未认证"
				} else if(da.status == 1) {
					status = "已认证"
				}
				
				$('#userName').html(da.userName);
				$('#phoneNumber').html(da.user.phone);
				$('#identityNum').html(da.identityNum);
				$('#confidence').html(da.confidence);
				$('#status').html(status);
				$('#phoneNumber').html(da.user.referee);
				document.getElementById("bigidentityFront").src = da.identityFront;
				document.getElementById("bigidentityBack").src = da.identityBack;
				document.getElementById("bigfaceUrl").src = da.faceUrl;
				
				$('img').zoomify();
			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});

}

//身份证信息
function baseMsg() {
	$.ajax({
		url: urlcore + "/api/userBasicMsg/selectOneDetailsByUserId?id=" + id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var n = data.data;
				$('#channelName').html(n.user.channelName);
				var phone1 = '<a  class="btn"   href="orderList.html?phone=' + phone+ '">'+phone+ '</a>';
				$("#phone").html(phone1);
				$("#realName").html(userName);
				$("#gmtDatetime").html(n.gmtDatetime);
				$("#score").html(n.user.authScore);
				$("#marry").html(n.marry);
				$("#study").html(n.study);
				$("#province").html(n.province);
				$("#city").html(n.city);
				$("#county").html(n.county);
				$("#areaCode").html(n.areaCode);
				$("#addressDetail").html(n.addressDetails);
				$("#workCompany").html(n.workCompany);
				$("#workPlace").html(n.workPlace);
				$("#workMoney").html(n.workMoney);
				$("#workPhone").html(n.workPhone);
				$("#linkPersonNameOne").html(n.linkPersonNameOne);
				$("#linkPersonPhoneOne").html(n.linkPersonPhoneOne);
				$("#linkPersonRelationOne").html(n.linkPersonRelationOne);
				$("#linkPersonNameTwo").html(n.linkPersonNameTwo);
				$("#linkPersonPhoneTwo").html(n.linkPersonPhoneTwo);
				$("#linkPersonRelationTwo").html(n.linkPersonRelationTwo);
				$("#confidence").html(n.confidence);
				$('#referee11').html(n.user.referee);
				$('#applyAddress111').html(n.user.applyAddress);
			} else if(data.code == 'OVERTIME') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}

			} else if(data.code == 'PARAMETER_INVALID') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}
			} else {
				alert(data.msg);
			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
}

//银行卡信息
function bankCardMsg() {
	$.ajax({
		url: urlcore + "/api/userBank/selectByUserId?id=" + id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var n = data.data;
				$("#bankcardno").html(n.bankcardno);
				$("#name").html(n.name);
				$("#idcardno").html(n.idcardno);
				$("#bankPhone").html(n.bankPhone);
				$('#cardname').html(n.bankName)
			} else if(data.code == 'OVERTIME') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}

			} else if(data.code == 'PARAMETER_INVALID') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}
			} else {
				alert(data.msg);
			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
}




//信贷保镖贷前审核信息
function creditAuditMsg() {
	$(document).ready(function() {
		//设置默认第1页
		init(1);
	});

	//默认加载  
	function init(pageNo) {
		//获取用户信息列表
		$("#thislist11").html("");
		$.ajax({
			url: urlcore + "/api/userTongdunfen/findByUserPage?orderId=" + orderId + "&pageNo=" + pageNo + "&pageSize=" + pageSize,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data.list, function(i, n) {
						var thislist11 = '<tr>' +
							'                                    <td class="footable-visible">' + n.id + '</td>' +
							'                                    <td class="footable-visible">' + n.riskName + '</td>' +
							'                                    <td class="footable-visible">' + n.score + '</td>' +
							'                                    <td class="footable-visible">' + n.orderNumber + '</td>' +
							'                                    <td class="footable-visible">' + n.gmtDatetime + '</td>' +		
							'                                </tr>';
						$('#thislist11').append(thislist11);
					});

					$("#pager11").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});

				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				
				alert("error");
			}
		});
	}

	//回调函数  
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}

//设备指纹信息
function sbzwMsg() {
	$.ajax({
		url: urlcore + "/api/tdSbzw/selectByOrderId?orderId=" + orderId,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var n = data.data;
				$("#idCardAddress").html(n.idCardAddress);
				$("#trueIpAddress").html(n.trueIpAddress);
				$("#wifiAddress").html(n.wifiAddress);
				$("#cellAddress").html(n.cellAddress);
				$("#bankCardAddress").html(n.bankCardAddress);
				$("#mobileAddress").html(n.mobileAddress);
				$("#fpVersion").html(n.fpVersion);
				$("#tokenId").html(n.tokenId);
				$("#deviceId").html(n.deviceId);
				$("#os").html(n.os);
				$("#osVersion").html(n.osVersion);
				$("#idfa").html(n.idfa);
				$("#idfv").html(n.idfv);
				$("#uuid").html(n.uuid);
				$("#bootTime").html(n.bootTime);
				$("#currentTime").html(n.currentTime);
				$("#upTime").html(n.upTime);
				$("#totalSpace").html(n.totalSpace);
				$("#freeSpace").html(n.freeSpace);
				$("#memory").html(n.memory);
				$("#brightness").html(n.brightness);
				$("#batteryStatus").html(n.batteryStatus);
				$("#batteryLevel").html(n.batteryLevel);
				$("#cellIp").html(n.cellIp);
				$("#wifiIp").html(n.wifiIp);
				$("#wifiNetmask").html(n.wifiNetmask);
				$("#mac").html(n.mac);
				$("#ssid").html(n.ssid);
				$("#bssid").html(n.bssid);
				$("#vpnIp").html(n.vpnIp);
				$("#vpnNetmask").html(n.vpnNetmask);
				$("#networkType").html(n.networkType);
				$("#proxyType").html(n.proxyType);
				$("#proxyUrl").html(n.proxyUrl);
				$("#platform").html(n.platform);
				$("#deviceName").html(n.deviceName);
				$("#carrier").html(n.carrier);
				$("#countryIso").html(n.countryIso);
				$("#mcc").html(n.mcc);
				$("#mnc").html(n.mnc);
				$("#bundleId").html(n.bundleId);
				$("#appVersion").html(n.appVersion);
				$("#timeZone").html(n.timeZone);
				$("#signMd5").html(n.signMd5);
				$("#languages").html(n.languages);
				$("#kernelVersion").html(n.kernelVersion);
				$("#gpsLocation").html(n.gpsLocation);
				$("#gpsSwitch").html(n.gpsSwitch);
				$("#gpsAuthStatus").html(n.gpsAuthStatus);
				$("#env").html(n.env);
				$("#attached").html(n.attached);
				$("#trueIp").html(n.trueIp);
				$("#appOs").html(n.appOs);
				$("#error").html(n.error);
				$("#position").html(n.position);
				$("#isp").html(n.isp);
				$("#latitude").html(n.latitude);
				$("#longitude").html(n.longitude);

			} else if(data.code == 'OVERTIME') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}

			} else if(data.code == 'PARAMETER_INVALID') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}
			} else {
				alert(data.msg);
			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
}

//通讯录信息
function addressListMsg() {
	$(document).ready(function() {
		//设置默认第1页
		init(1);
	});

	//默认加载  
	function init(pageNo) {
		//获取用户信息列表
		$("#thislist3").html("");
		$.ajax({
			url: urlcore + "/api/userPhoneList/findByUserPage?userId=" + id + "&pageNo=" + pageNo + "&pageSize=" + pageSize,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data.list, function(i, n) {
						var thislist3 = '<tr>' +
							'<td class="footable-visible">' + n.id + '</td>' +
							'<td class="footable-visible">' + n.name + '</td>' +
							'<td class="footable-visible">' + n.phone + '</td>' +
							'<td class="footable-visible">' + n.link + '</td>' +
							'</tr>';
						$('#thislist3').append(thislist3);
					});

					$("#pager3").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});

				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
	}

	//回调函数  
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}


//跳转到数据魔盒报告
function phoneMsg() {

	var taskId = "";
	var userName = "";
	var new_window = window.open("","","width=1000,height=800");
	$.ajax({
			url: urlcore + "/api/user/selectYYSResult?userId=" + id ,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				taskId = data.data.taskId;
				userName = data.data.mobile;
				new_window .location = "https://portal.shujumohe.com/main/customerReport/"+taskId+"?user_name="+userName;
			},
			error: function() {
				
				alert("error");
			}
		});

}
//跳转天创淘宝报告
function tbMsg() {

	var appId = "858a17fe-3572-4a9d-aba9-b2d1ab4cc088";
	var tid = "";
	var new_window = window.open("","","width=1000,height=800");
	$.ajax({
			url: urlcore + "/api/user/getTid?userId=" + id ,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				tid = data.data.taskId;
				new_window .location = "http://fc.tcredit.com/reports/dsReportPage?tid="+tid+"&appId="+appId;
			},
			error: function() {
				
				alert("error");
			}
		});

}

//淘宝收货地址
function taobaoAddressMsg(){
	$("#addList").html("");
	$.ajax({
			url: urlcore + "/api/tcTaobaoAddress/selectListByUserId?userId=" + id ,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data, function(i, n) {
						var isD = "否";
						if(n.isDefaultAddress == "1"){
							isD = "是";
						}
						var thislist3 = '<tr>' +
							'<td class="footable-visible">' + n.receiverName + '</td>' +
							'<td class="footable-visible">' + n.receiverPhone + '</td>' +
							'<td class="footable-visible">' + n.province + '</td>' +
							'<td class="footable-visible">' + n.city + '</td>' +
							'<td class="footable-visible">' + n.receiverTitle + '</td>' +
							'<td class="footable-visible">' + n.receiverAddr + '</td>' +
							'<td class="footable-visible">' + n.receiverRegion + '</td>' +
							'<td class="footable-visible">' + n.zipcode + '</td>' +
							'<td class="footable-visible">' + isD + '</td>' +
							'</tr>';
						$('#addList').append(thislist3);
					});


				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
}

//淘宝信息
function taobaoMsg(){
	$.ajax({
		url: urlcore + "/api/tcTaobaoBaseInfo/selectOne?userId=" + id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var n = data.data;
				$("#alipayAcc").html(n.alipayAcc);
				$("#alipayAccType").html(n.alipayAccType);
				$("#alipayBalance").html(n.alipayBalance);
				$("#alipayRegDate").html(n.alipayRegDate);
				$("#antMemberScore").html(n.antMemberScore);
				$("#balancePaymentEnable").html(n.balancePaymentEnable);
				$("#creditLevelAsBuyer").html(n.creditLevelAsBuyer);
				$("#creditLevelAsSeller").html(n.creditLevelAsSeller);
				$("#isVerified").html(n.isVerified);
				$("#taobaoAccount").html(n.taobaoAccount);
				$("#taobaoScore").html(n.taobaoScore);
				$("#tmallScore").html(n.tmallScore);
				$("#yuebaoBalance").html(n.yuebaoBalance);
				$("#yuebaoIncome").html(n.yuebaoIncome);
				$("#nickName").html(n.nickName);
				$("#tbRealName").html(n.tbRealName);
				$("#aliRealName").html(n.aliRealName);
				$("#email").html(n.email);
				$("#cellPhone").html(n.cellPhone);
				$("#securityLevel").html(n.securityLevel);
				$("#level").html(n.level);
				$("#websiteId").html(n.websiteId);
				$("#cardCode").html(n.cardCode);
			} else if(data.code == 'OVERTIME') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}

			} else if(data.code == 'PARAMETER_INVALID') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}
			} else {
				alert(data.msg);
			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
	
	$.ajax({
		url: urlcore + "/api/tcTaobaoHuabei/selectOne?userId=" + id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var n = data.data;
				$("#amount").html(n.amount);
				$("#balance").html(n.balance);
				$("#originalAmount").html(n.originalAmount);
				$("#overdueDays").html(n.overdueDays);
				$("#penaltyAmount").html(n.penaltyAmount);
				$("#payDay").html(n.payDay);
				$("#overdueBillCnt").html(n.overdueBillCnt);
				$("#currentMonthPayment").html(n.currentMonthPayment);
				$("#nextMonthPayment").html(n.nextMonthPayment);
				$("#freeze").html(n.freeze);
			} else if(data.code == 'OVERTIME') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}

			} else if(data.code == 'PARAMETER_INVALID') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}
			} else {
				alert(data.msg);
			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
	
	$.ajax({
		url: urlcore + "/api/tcTaobaoJiebei/selectOne?userId=" + id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var n = data.data;
				$("#amount1").html(n.amount);
				$("#balance1").html(n.balance);
				$("#riskRate").html(n.riskRate);
				$("#isOverdue").html(n.isOverdue);
				$("#isNewAble").html(n.isNewAble);
				$("#isClosed").html(n.isClosed);
				$("#unClearLoanCount").html(n.unClearLoanCount);
				$("#isAvailable").html(n.isAvailable);
			} else if(data.code == 'OVERTIME') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}

			} else if(data.code == 'PARAMETER_INVALID') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}
			} else {
				alert(data.msg);
			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
}
//花呗消费记录
function taobaoHuaBei(){
	$("#addList").html("");
	$.ajax({
			url: urlcore + "/api/tcTaobaoHuabeiConsumes/selectListByUserId?userId=" + id ,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data, function(i, n) {
						var thislist3 = '<tr>' +
							'<td class="footable-visible">' + n.amount + '</td>' +
							'<td class="footable-visible">' + n.type + '</td>' +
							'<td class="footable-visible">' + n.name + '</td>' +
							'<td class="footable-visible">' + n.tradeNo + '</td>' +
							'<td class="footable-visible">' + n.bizType + '</td>' +
							'<td class="footable-visible">' + n.billMonth + '</td>' +
							'</tr>';
						$('#hbList').append(thislist3);
					});


				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
}

//学信信息
function xxMsg() {
	$.ajax({
		url: urlcore + "/api/tdSchoolInfo/selectOne?id=" + id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var n = data.data;
				$("#realname").html(n.realname);
				$("#gender").html(n.gender);
				$("#nation").html(n.nation);
				$("#birthday").html(n.birthday);
				$("#card_id").html(n.cardId);
				$("#examination_id").html(n.examinationId);
				$("#student_id").html(n.studentId);
				$("#school").html(n.school);
				$("#college").html(n.college);
				$("#department").html(n.department);
				$("#major").html(n.major);
				$("#classname").html(n.classname);
				$("#edu_level").html(n.eduLevel);
				$("#edu_system").html(n.eduSystem);
				$("#edu_type").html(n.eduType);
				$("#edu_form").html(n.eduForm);
				$("#entrance_date").html(n.entranceDate);
				$("#graduate_date").html(n.graduateDate);
				$("#status").html(n.status);
				document.getElementById("head_img").src = n.headImg;

			} else if(data.code == 'OVERTIME') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}

			} else if(data.code == 'PARAMETER_INVALID') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}
			} else {
				alert(data.msg);
			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
	
	$.ajax({
		url: urlcore + "/api/tdEducationInfo/selectOne?id=" + id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var n = data.data;
				
				$("#realname1").html(n.realname);
				$("#gender1").html(n.gender);
				$("#birthday1").html(n.birthday);
				$("#entrance_date1").html(n.entranceDate);
				$("#graduate_date1").html(n.graduateDate);
				$("#location1").html(n.location);
				$("#school1").html(n.school);
				$("#major1").html(n.major);
				$("#edu_type1").html(n.eduType);
				$("#edu_level1").html(n.eduLevel);
				$("#edu_form1").html(n.eduForm);
				$("#certificate_id1").html(n.certificateId);
				$("#edu_conclusion1").html(n.eduConclusion);
				document.getElementById("head_img1").src = n.headImg;
			} else if(data.code == 'OVERTIME') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}

			} else if(data.code == 'PARAMETER_INVALID') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}
			} else {
				alert(data.msg);
			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
}

//京东基础信息
function jdBaseInfo() {
	$.ajax({
		url: urlcore + "/api/tdJdBaseInfo/selectOne?id=" + id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var n = data.data;
				$("#user_name").html(n.userName);
				$("#email").html(n.email);
				$("#user_level").html(n.userLevel);
				$("#nick_name").html(n.nickName);
				$("#namejd").html(n.name);
				$("#gender").html(n.gender);
				$("#mobile").html(n.mobile);
				$("#real_name").html(n.realName);
				$("#identity_code").html(n.identityCode);
				$("#vip_count").html(n.vipCount);
				$("#account_balance").html(n.accountBalance);
				$("#financial_account_balance").html(n.financialAccountBalance);
				$("#credit_point").html(n.creditPoint);
				$("#credit_quota").html(n.creditQuota);
				$("#consume_quota").html(n.consumeQuota);

			} else if(data.code == 'OVERTIME') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}

			} else if(data.code == 'PARAMETER_INVALID') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}
			} else {
				alert(data.msg);
			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
}

//京东收货地址
function jdReceiver(){
	$("#jdAddList").html("");
	$.ajax({
			url: urlcore + "/api/tdJdReceiverList/selectList?id=" + id ,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data, function(i, n) {
						var isD = "否";
						if(n.defaultadd == 1){
							isD = "是";
						}
						var thislist3 = '<tr>' +
							'<td class="footable-visible">' + n.name + '</td>' +
							'<td class="footable-visible">' + n.area + '</td>' +
							'<td class="footable-visible">' + n.address + '</td>' +
							'<td class="footable-visible">' + n.mobile + '</td>' +
							'<td class="footable-visible">' + n.telephone + '</td>' +
							'<td class="footable-visible">' + isD + '</td>' +
							'<td class="footable-visible">' + n.zipCode + '</td>' +
							'</tr>';
						$('#jdAddList').append(thislist3);
					});


				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
}

//京东订单信息
function jdOrder(){
	$("#jdOrderList").html("");
	$.ajax({
			url: urlcore + "/api/tdJdOrderList/selectList?id=" + 1050 ,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data, function(i, n) {
						var thislist3 = '<tr>' +
							'<td class="footable-visible">' + n.orderId + '</td>' +
							'<td class="footable-visible">' + n.orderAmount + '</td>' +
							'<td class="footable-visible">' + n.orderType + '</td>' +
							'<td class="footable-visible">' + n.orderTime + '</td>' +
							'<td class="footable-visible">' + n.orderStatus + '</td>' +
							'<td class="footable-visible">' + n.orderShop + '</td>' +
							'<td class="footable-visible">' + n.receiverName + '</td>' +
							'<td class="footable-visible">' + n.receiverAddr + '</td>' +
							'<td class="footable-visible">' + n.receiverMobile + '</td>' +
							'<td class="footable-visible">' + n.receiverTelephone + '</td>' +
							'<td class="footable-visible">' + n.receiverZipCode + '</td>' +
							'<td class="footable-visible">' + n.receiptTitle + '</td>' +
							'<td class="footable-visible">' + n.receiptType + '</td>' +
							'<td class="footable-visible">' + n.receiptContent + '</td>' +
							'</tr>';
						$('#jdOrderList').append(thislist3);
					});


				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
}


function loadMyEssaygjj(){
	$.ajax({
		url: urlcore + "/api/tdGjjBaseInfo/selectOne?id=" + id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {
				var n = data.data;
				var cert_type11="未知";
				if(n.certType == 1){
					cert_type11 = "身份证";
				}else if(n.certType == 2){
					cert_type11 = "护照";
				}
				
				
				$("#cust_no").html(n.custNo);
				$("#pay_status").html(n.payStatus);
				$("#pay_status_desc").html(n.payStatusDesc);
				$("#name").html(n.name);
				$("#cert_no").html(n.certNo);
				$("#cert_type").html(cert_type11);
				$("#home_address").html(n.homeAddress);
				$("#post_no").html(n.postNo);
				$("#mobile").html(n.mobile);
				$("#email").html(n.email);
				$("#corp_name").html(n.corpName);
				$("#monthly_corp_income").html(n.monthlyCorpIncome/100);
				$("#monthly_corp_proportion").html(n.monthlyCorpProportion);
				$("#monthly_cust_income").html(n.monthlyCustIncome/100);
				$("#monthly_cust_proportion").html(n.monthlyCustProportion);
				$("#monthly_total_income").html(n.monthlyTotalIncome/100);
				$("#base_number").html(n.baseNumber/100);
				$("#balance").html(n.balance/100);
				$("#last_pay_date").html(n.lastPayDate);
				$("#begin_date").html(n.beginDate);
				$("#corp_no").html(n.corpNo);
				$("#education").html(n.education);
				$("#registed").html(n.registed);
				$("#name_of_spouse").html(n.nameOfSpouse);
				$("#cert_no_of_spouse").html(n.certNoOfSpouse);
				$("#marital_status").html(n.maritalStatus);
			} else if(data.code == 'OVERTIME') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}

			} else if(data.code == 'PARAMETER_INVALID') {
				var thisUrl = window.location.href;
				if(thisUrl.indexOf('login.html') <= -1) {
					top.window.location.href = "login.html";
				}
			} else {
				alert(data.msg);
			}

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
	
	$("#thislistgjj1").html("");
		$.ajax({
			url: urlcore + "/api/tdGjjPayInfo/findByUserPage?userId=" + id + "&pageNo=" + pageNo + "&pageSize=" + pageSize,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data.list, function(i, n) {
						
						
						var thislist3 = '<tr>' +
							'<td class="footable-visible">'+n.gjjNo+'</td>'+
							'<td class="footable-visible">'+n.corpNo+'</td>'+
							'<td class="footable-visible">'+n.corpName+'</td>'+
							'<td class="footable-visible">'+n.balance/100+'</td>'+
							'<td class="footable-visible">'+n.payStatus+'</td>'+
							'<td class="footable-visible">'+n.payStatusDesc+'</td>'+
							'                                </tr>';
						$('#thislistgjj1').append(thislist3);
					});

	
				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
		$("#thislistgjj2").html("");
		$.ajax({
			url: urlcore + "/api/tdGjjBillRecord/findByUserPage?userId=" + id + "&pageNo=" + pageNo + "&pageSize=" + pageSize,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data.list, function(i, n) {
						var thislist3 = '<tr>' +
							'<td class="footable-visible">'+n.dealTime+'</td>'+
							'<td class="footable-visible">'+n.corpName+'</td>'+
							'<td class="footable-visible">'+n.corpIncome/100+'</td>'+
							'<td class="footable-visible">'+n.custIncome/100+'</td>'+
							'<td class="footable-visible">'+n.income/100+'</td>'+
							'<td class="footable-visible">'+n.outcome/100+'</td>'+
							'<td class="footable-visible">'+n.desc+'</td>'+
							'<td class="footable-visible">'+n.balance/100+'</td>'+
							'</tr>';
						$('#thislistgjj2').append(thislist3);
					});


				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
}










function loadMyEssay1() {
	$(document).ready(function() {
		//设置默认第1页
		init(1);
		base();
		
	});

	function base(){
		$.ajax({
			url: urlcore + "/api/userTaobaoAddress/findUserTBBaseInfo?userId=" + id,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					var da = data.data;
					$("#tb_userName").html(da.userName);
					$("#tb_nickName").html(da.nickName);
					$("#tb_name").html(da.name);
					$("#tb_gender").html(da.gender);
					$("#tb_mobile").html(da.mobile);
					$("#tb_realName").html(da.realName);
				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
	}
	

	//默认加载  
	function init(pageNo) {
		//获取用户信息列表
		$("#thislist1").html("");
		$.ajax({
			url: urlcore + "/api/userTaobaoAddress/findByAddressPage?pageNo=" + pageNo + "&pageSize=" + pageSize + "&userId=" + id,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data.list, function(i, n) {
						var isDefault = "";
						if("1" == n.defaultArea){
							isDefault = "默认地址";
						}
						var thislist1 = '<tr>' +
							'<td class="footable-visible">' + n.id + '</td>' +
							'<td class="footable-visible">' + n.name + '</td>' +
							'<td class="footable-visible">' + n.area + '</td>' +
							'<td class="footable-visible">' + n.address + '</td>' +
							'<td class="footable-visible">' + n.mobile + '</td>' +
							'<td class="footable-visible">' + isDefault + '</td>' +
							'</tr>';
						$('#thislist1').append(thislist1);
					});

					$("#pager1").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});

				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
	}

	//回调函数  
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}

//function loadMyEssay2() {
//	$(document).ready(function() {
//		//设置默认第1页
//		init(1);
//	});
//
//	//默认加载  
//	function init(pageNo) {
//		//获取用户信息列表
//		$("#thislist2").html("");
//		$.ajax({
//			url: urlcore + "/api/userJindongAddress/findByAddressPage?pageNo=" + pageNo + "&pageSize=" + pageSize + "&userId=" + id,
//			type: "get",
//			dataType: 'json',
//			contentType: "application/json;charset=utf-8",
//			success: function(data) {
//				if(data.success == true) {
//					//i表示在data中的索引位置，n表示包含的信息的对象
//					$.each(data.data.list, function(i, n) {
//						var thislist2 = '<tr>' +
//							'                                    <td class="footable-visible">' + n.id + '</td>' +
//							'                                    <td class="footable-visible">' + phone + '</td>' +
//							'                                    <td class="footable-visible">' + userName + '</td>' +
//							'                                    <td class="footable-visible">' + n.orderAddress + '</td>' +
//							'                                </tr>';
//						$('#thislist2').append(thislist2);
//					});
//
//					$("#pager2").pager({
//						pagenumber: pageNo,
//						pagecount: data.data.pages,
//						totalcount: data.data.total,
//						buttonClickCallback: PageClick
//					});
//
//				} else if(data.code == 'OVERTIME') {
//					var thisUrl = window.location.href;
//					if(thisUrl.indexOf('login.html') <= -1) {
//						top.window.location.href = "login.html";
//					}
//
//				} else {
//					if(data.msg != '空数据') {
//						alert(data.msg)
//					} else {
//						$('#thiscount').text(0);
//					}
//				}
//
//			},
//			error: function() {
//				/* Act on the event */
//				alert("error");
//			}
//		});
//	}
//
//	//回调函数  
//	PageClick = function(pageclickednumber) {
//		init(pageclickednumber);
//	}
//
//}




function loadMyEssay6() {
	$(document).ready(function() {
		//设置默认第1页
		init(1);
	});

	//默认加载  
	function init(pageNo) {
		//获取用户信息列表
		$("#thislist6").html("");
		$.ajax({
			url: urlcore + "/api/userTaobaoZhifubao/findByUserPage?userId=" + id + "&pageNo=" + pageNo + "&pageSize=" + pageSize,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data.list, function(i, n) {
						var thislist3 = '<tr>' +
							'                                    <td class="footable-visible">' + n.id + '</td>' +
							'                                    <td class="footable-visible">' + userName + '</td>' +
							'                                    <td class="footable-visible">' + n.huabeiCanUseMoney + '</td>' +
							'                                    <td class="footable-visible">' + n.huabeiTotalAmount + '</td>' +
							'                                    <td class="footable-visible">' + n.alipayRemainingAmount + '</td>' +
						
							'                                </tr>';
						$('#thislist6').append(thislist3);
					});

					$("#pager6").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});

				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;
					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				
				alert("error");
			}
		});
	}

	//回调函数  
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}

//function test(){
//             
//			var  td_data= null;
//			
//
//     		var  person_info= null;
//     		
//     		$.ajax({
//			url: urlcore + "/api/mobileAuthentication/query?userId ="+id,
//			type: "get",
//			dataType: 'json',
//			contentType: "application/json;charset=utf-8",
//			success: function(data) {
//				if(data.success == true) {
//					
//					td_data = data.data.td_data;
//					person_info = data.data.person_info;
//					
//
//			},
//			error: function() {
//				/* Act on the event */
//				alert("error");
//			}
//			});
//     		alert(td_data);
//     		alert(person_info);
//     		$.showTDReport(td_data,person_info)
//}
        
        function test()
        {
             
			var  td_data= null;

       		var  person_info=null;
       		$.ajax({
				url: urlcore + "/api/mobileAuthentication/query?userId="+id,
				type: "get",
				dataType: 'json',
				contentType: "application/json;charset=utf-8",
				success: function(data) {
					if(data.success == true) {
						if("undefined" == typeof(data.data.td_data1)){
							alert("同盾分为"+data.data.score);
						}else{
							td_data = data.data.td_data1;
							person_info = data.data.person_info1;
	       					$.showTDReport(td_data,person_info)
						}
						
					}
				},
				error: function() {
					/* Act on the event */
					alert("error");
				}
				});
			
       		
        }
        
        (function(){
            if(typeof jQuery == "undefined"){
                var  jq_script = document.createElement('script');
                jq_script.type = "text/javascript";
                jq_script.src =  "http://lib.sinaapp.com/js/jquery/1.9.1/jquery-1.9.1.min.js";
                jq_script.onload = loadPreloanLib;
                document.getElementsByTagName('head')[0].appendChild(jq_script);
            } else {
                loadPreloanLib();
            }
         
         
            function loadPreloanLib(){
                var td_script = document.createElement('script');
                td_script.type = "text/javascript";
                td_script.src = "http://cdnjs.tongdun.cn/preloan/tdreport.1.4.min.js?r=" + (new Date()).getTime();
                document.getElementsByTagName('head')[0].appendChild(td_script);
            }
		})();


