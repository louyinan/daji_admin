var authStatus = '';
var phoneNumber = '';
var userName = '';
var currentPage = 1;
var jName = getCookie('Jname');
//我的权限数组
var arrayTitle = new Array; 
loadMyEssay(authStatus, phoneNumber, userName);

function loadMyEssay(authStatus, phoneNumber, userName) {

	$(document).ready(function() {
		findMyCatalogue()
		init(currentPage);
	});

	function init(pageNo) {
		$("#thislist").html("");
		$.ajax({
			url: urlcore + "/api/user/selectUserList?authStatus=" + authStatus + "&phone=" + phoneNumber + "&userName=" + userName + "&current=" + pageNo,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {

				if(data.success == true) {
					$.each(data.data.list, function(i, n) {
						var status = '';
						if(n.status == 1) {
							status = '正常';
						} else if(n.status == 2) {
							status = '黑名单';
						} else if(n.status == 3) {
							status = '禁用';
						}
						var authstatus = ""; //认证状态
						if(n.authStatus == 0) {
							authstatus = "未认证";
						} else if(n.authStatus == 1) {
							authstatus = "认证成功";
						}
						var id = n.id;
						var addr = "";
						if(n.userBasicMsg !=null){
							if(n.userBasicMsg.province != null){
								addr = addr + n.userBasicMsg.province;
							}
							if(n.userBasicMsg.city != null){
								addr = addr + n.userBasicMsg.city;
							}
							if(n.userBasicMsg.county != null){
								addr = addr + n.userBasicMsg.county;
							}
						}
						
						var thislist =
							'<tr class="footable-even" style="display: table-row;">' +
							'<td class="footable-visible"><input type="checkbox" /></td>' +
							'<td class="footable-visible">' + n.id + '</td>' +
							'<td class="footable-visible">' + n.userName + '</td>' +
							'<td class="footable-visible">' +'<a  class="btn"   href="orderList.html?phone=' + n.phone+ '">'+n.phone+ '</a></td>' +
							
							'<td class="footable-visible">' + addr + '</td>' +
							'<td class="footable-visible">' + n.channelName + '</td>' +
							'<td class="footable-visible">' + n.referee + '</td>' +
							
							'<td class="footable-visible">' + n.gmtDatetime + '</td>' +
							'<td class="footable-visible">' + status + '</td>' +
							'<td class="footable-visible">' + authstatus + '</td>' +
							
							'<td class="footable-visible footable-last-column">'+
								'<a hidden="hidden" class="" name="拉黑" href="javascript:;" data onclick="thisUpdate(' + id + ')">拉黑</a>&nbsp;'+
								'<a hidden="hidden" class="" name="禁用" href="javascript:;" data onclick="thisDelete(' + id + ')">禁用</a>&nbsp;' +
								'<a hidden="hidden" class="" name="设置渠道" href="javascript:;" data-toggle="modal" data-target="#channelAdd" onclick="selectChannel('+id+');">设置渠道</a></td>' +
							'</tr>';
						$('#thislist').append(thislist);
					});
					$.each(arrayTitle, function(i,k) {
						$('a[name="'+k+'"]').attr("hidden",false).attr("class","btn btn-primary btn-xs");
					});
					$("#pager").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});

				} else if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;

					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}

			},
			error: function() {
				alert("error");
			}
		});
	}
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}
}
var userId ;
function selectChannel(id){
	userId = id;
	var str = '<option value ="">渠道商</option>';
	$("#channelList").html(str);
	$.ajax({
			url: urlcore + "/api/channel/selectList?pageNo=1&pageSize=1000&loginName="+"",
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success:function(data){
			     if(data.success == true) {
			     	$.each(data.data.list, function(i, n) {
			     		var str = '<option value ="'+n.id+'">'+n.name+'</option>';
			     		$("#channelList").append(str);
			     	});
			     }
			},
			error:function() {
				/* Act on the event */
				alert("error");
			}
		});	
}

function channelAddSave(){
	var channelId = $("#channelList").val(); 
	var channelName = $("#channelList").find("option:selected").text(); 
	$.ajax({
			url: urlcore + "/api/channel/setUserChannel?channelId="+channelId+"&channelName="+channelName+"&userId="+userId,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success:function(data){
			     if(data.success == true) {
			     	location.reload();
			     	alert("保存成功");
			     }
			},
			error:function() {
				/* Act on the event */
				alert("error");
			}
		});	
}
function EnterSearchList() {
	var code = event.keyCode;
	if(code == 13) {
		searchList();
	}

}

function searchList() {

	var phoneNumber = $('#phoneNumber').val().trim();
	var userName = $('#userName').val().trim();
	
	loadMyEssay('',phoneNumber,userName);


}


function selectOrdersStatus(authstatus){
	var authstatus = $('#authStatus').val();
	loadMyEssay(authstatus,'','');
	
}

function thisDelete(id) {
		if(confirm("您确定要禁用该用户吗？")){
		$.ajax({
			url: urlcore + "/api/user/GoDeleteUpdate?id="+id,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success:function(data){
			     loadMyEssay('','','');
			},
			error:function() {
				/* Act on the event */
				alert("error");
			}
		});	
}
}
function thisUpdate(id) {
	if(confirm("您确定要删除该用户吗？")){
		$.ajax({
			url: urlcore + "/api/user/GoDeleteUpdate1?id="+id,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success:function(data){
			     loadMyEssay('','','');
			},
			error:function() {
				/* Act on the event */
				alert("error");
			}
		});	
}
}

function findMyCatalogue(){
	$.ajax({
		url: urlcore + "/api/roleThirdCatalogue/findAllByUser?secondTitle="+jName,
		type: "GET",
		dataType: 'json',
		async: false,
		contentType: "application/json;charset=utf-8",
		success:function(data){
		if (data.success == true) {
			$.each(data.data, function(i,n) {
				arrayTitle.push(n.thirdCatalogue.title);
			});
		} else {
			alert(data.msg);
		}

		},
		error:function() {
			alert("error");
		}
	});
}