var status = -1;
var icloudNo="";
//我的权限数组
var arrayTitle = new Array; 
$(function(){
	loadMyEssay();
});


function loadMyEssay() {		
	$(document).ready(function() {
		initMySummernote("faileContent");
		initMySummernote("faileContentDetails");
//		findMyCatalogue();
		//设置默认第1页
	    init(1); 
	}); 
	
	//默认加载  
	function init(pageNo){
		//获取用户信息列表
		$("#thislist").html(""); 
		$.ajax({
			url: urlcore + "/api/withdrawChannel/adminFindByPage?currentPage="+pageNo+"&status="+status,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success:function(data){
				if (data.success == true) { 
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data.list,function(i,n){
						var statusTitle;
						if (n.status == 1) {
							statusTitle = "审核中";
						}else if(n.status == 2){
							statusTitle = "审核失败";
						}else if(n.status == 3){
							statusTitle = "打款成功";
						}
						var thislist = '<tr>'+
'                                    <td class="footable-visible">'+n.id+'</td>'+
'                                    <td class="footable-visible">'+n.money+'</td>'+
'                                    <td class="footable-visible">'+n.bankNumber+'</td>'+
'                                    <td class="footable-visible">'+n.userName+'</td>'+
'                                    <td class="footable-visible">'+n.gmtDatetime+'</td>'+
'                                    <td class="footable-visible">'+statusTitle+'</td>'+
'									 <td class="footable-visible footable-last-column">';
						if (n.status == 1) {
									thislist+='<a name="通过/已打款" class="btn btn-primary btn-xs" onclick="agree('+n.id+')">通过/已打款</a>&nbsp;'+
									'<a name="拒绝" class="btn btn-primary btn-xs" onclick="agreeNoBefore('+n.id+')" href="javascript:;" data-toggle="modal" data-target="#update">拒绝</a>&nbsp;'
						}else if(n.status == 2){
									thislist+='<a name="失败原因" class="btn btn-primary btn-xs"  onclick="details('+n.id+')" href="javascript:;" data-toggle="modal" data-target="#details">失败原因</a>&nbsp;';
						}
									thislist+='</td>' +
'                                </tr>';
						$('#thislist').append(thislist);
					});
					$.each(arrayTitle, function(i,k) {
						$('a[name="'+k+'"]').attr("hidden",false).attr("class","btn btn-primary btn-xs");
						$('a[data="topButton"]').attr("class","btn btn-sm btn-primary");
					});
					$('#thiscount').text(data.data.total);
					$("#pager").pager({
					pagenumber: pageNo, 
					pagecount:data.data.pages,
					totalcount:data.data.total,
					buttonClickCallback: PageClick
					}); 
					
				} else if (data.code == 'OVERTIME'){
					var thisUrl = window.location.href;
					if (thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href="login.html";
					}

				} else {
					if (data.msg != '空数据') {
						alert(data.msg)
					}else{
						$('#thiscount').text(0);
					}
				}

			},
			error:function() {
				/* Act on the event */
				alert("error");
			}
		});
	}

	//回调函数  
	PageClick = function(pageclickednumber) {  
	    init(pageclickednumber); 
	}

}

function findMyCatalogue(){
	$.ajax({
		url: urlcore + "/api/roleThirdCatalogue/findAllByUser?secondTitle="+jName,
		type: "GET",
		dataType: 'json',
		async: false,
		contentType: "application/json;charset=utf-8",
		success:function(data){
		if (data.success == true) {
			$.each(data.data, function(i,n) {
				arrayTitle.push(n.thirdCatalogue.title);
			});
		} else {
			alert(data.msg);
		}

		},
		error:function() {
			alert("error");
		}
	});
}


function searchList(){
	status = $('#status').val();
	loadMyEssay();
}

//审核失败之前操作
function agreeNoBefore(agreeId){
	$('#agreeId').attr("name",agreeId)
}

//审核失败之前操作
function agreeNo(id){
	var reason = $('#faileContent').summernote('code');
	if(confirm("确认？")) {
		$.ajax({
			url: urlcore + "/api/withdrawChannel/updateStatus?id="+id+'&status=2&reason='+escape(reason),
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				loadMyEssay();
			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
	}
}

//同意提现/已打款
function agree(id){
	if(confirm("确认？")) {
		$.ajax({
			url: urlcore + "/api/withdrawChannel/updateStatus?id="+id+'&status=3',
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				loadMyEssay();
			},
			error: function() {
				/* Act on the event */
				alert("error");
			}
		});
	}
}

//查询失败原因
function details(id){
	$.ajax({
		url: urlcore + "/api/withdrawChannel/selectOne?id="+id,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			$('#faileContentDetails').summernote("code",data.data.reason);
		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
}
