var gmtDatetime = '';
var name = '';
var phoneNumber = '';
var currentPage = 1;
var totalMoney = 0;
var totalPeople = 0;
var jName = getCookie('Jname');
var time ='';
//我的权限数组
var arrayTitle = new Array;

loadMyEssay(time, name, phoneNumber);

function loadMyEssay(time, name, phoneNumber) {

	$(document).ready(function() {
		findMyCatalogue()
		init(currentPage);
	});

	function init(pageNo) {
		$("#thislist").html("");
		var status = new Array();
		status.push(1);
		$.ajax({
			url: urlcore + "/api/yopPayLog/selectList?time=" + time + "&name=" + name + "&phone=" + phoneNumber + "&current=" + pageNo+ "&status="+status,
			type: "get",
			async: 'false',
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					$.each(data.data.list, function(i, n) {

						var id = n.id;
					
						var realPayTime = null;
						if(n.createTime != null){
							realPayTime = new Date(n.createTime).pattern("yyyy-MM-dd hh-mm-ss");
						}

//	var date = n.gmtDatetime.replace("-", "/")
						//alert(date);
						var thislist =
							'<tr class="footable-even" style="display: table-row;">' +
							'<td class="footable-visible">' + n.id + '</td>' +
							'<td class="footable-visible">' + n.name + '</td>' +
							'<td class="footable-visible">' + n.phone + '</td>' +
							'<td class="footable-visible">' + n.orderId + '</td>' +
							'<td class="footable-visible">' + n.money + '</td>' +
							'<td class="footable-visible">' + n.requestno + '</td>' +
							'<td class="footable-visible">' + n.result + '</td>' +
							'<td class="footable-visible">' + realPayTime + '</td>' +
							'</tr>';

						$('#thislist').append(thislist);
					});
					$.each(arrayTitle, function(i, k) {
						$('a[name="' + k + '"]').attr("hidden", false).attr("class", "btn btn-primary btn-xs");
					});
					$("#pager").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});

					if(data.code == 'OVERTIME') {
						var thisUrl = window.location.href;

						if(thisUrl.indexOf('login.html') <= -1) {
							top.window.location.href = "login.html";
						}

					} else {
						if(data.msg != '空数据') {
							//alert(data.msg)
						} else {
							$('#thiscount').text(0);
						}
					}
				}

			},
			error: function() {
				alert("error");
			}
		});
	}
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}

function EnterSearchList() {
	var code = event.keyCode;
	if(code == 13) {
		searchList();
	}
}

function searchList() {

	var phoneNumber = $('#phoneNumber').val().trim();
	var userName = $('#userName').val().trim();
	var time = $('#applyTime').val().trim();
	loadMyEssay(time, userName, phoneNumber);

}


function findMyCatalogue() {
	$.ajax({
		url: urlcore + "/api/roleThirdCatalogue/findAllByUser?secondTitle=" + jName,
		type: "GET",
		dataType: 'json',
		async: false,
		contentType: "application/json;charset=utf-8",
		success:function(data){
		if (data.success == true) {
			$.each(data.data, function(i,n) {
				arrayTitle.push(n.thirdCatalogue.title);
			});
		} else {
			alert(data.msg);
		}

		},
		error: function() {
			alert("error");
		}
	});
}

/*function myBrowser() {
	var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
	var isOpera = userAgent.indexOf("Opera") > -1;
	if(isOpera) {
		return "Opera"
	}; //判断是否Opera浏览器
	if(userAgent.indexOf("Firefox") > -1) {
		return "FF";
	} //判断是否Firefox浏览器
	if(userAgent.indexOf("Chrome") > -1) {
		return "Chrome";
	}
	if(userAgent.indexOf("Safari") > -1) {
		return "Safari";
	} //判断是否Safari浏览器
	if(userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
		return "IE";
	}; //判断是否IE浏览器
}
//以下是调用上面的函数
var mb = myBrowser();
if("IE" == mb) {
	alert("我是 IE");
}
if("FF" == mb) {
	alert("我是 Firefox");
}
if("Chrome" == mb) {
	alert("我是 Chrome");
}
if("Opera" == mb) {
	alert("我是 Opera");
}
if("Safari" == mb) {
	alert("我是 Safari");
}*/