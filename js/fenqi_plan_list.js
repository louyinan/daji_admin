var orderId = getvl("orderId")
//我的权限数组
var arrayTitle = new Array; 
init();



	function init(pageNo) {
		$("#thislist").html("");
		$.ajax({
			url: urlcore + "/api/user/getFenqiPlan?orderId=" +orderId,
			type: "get",
			async: 'false',
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					$.each(data.data, function(i, n) {

						var id = n.id;
						var status = '';
						if(n.fenqiOrderStatus == "3") {
							status = "待还款"
						} else if(n.fenqiOrderStatus == "4") {
							status = "容限期中";
						} else if(n.fenqiOrderStatus == "5") {
							status = "超出容限期中";
						} else if(n.fenqiOrderStatus == "6") {
							status = "已还款";
						} else if(n.fenqiOrderStatus == "8") {
							status = "坏账";
						}
						var limitPayTime = null;
						if(n.limitPayTime != null){
							limitPayTime = new Date(n.limitPayTime).pattern("yyyy-MM-dd");
						}
						var realPayTime = null;
						if(n.realPayTime != null){
							realPayTime = new Date(n.realPayTime).pattern("yyyy-MM-dd");
						}
//						var s = new Date(n.realPayTime).pattern("yyyy-MM-dd hh:mm:ss");
						var thislist =
							'<tr class="footable-even" style="display: table-row;">' +

							'<td class="footable-visible">' + n.id + '</td>' +
							'<td class="footable-visible">' + n.fenqiNo + '</td>' +
							'<td class="footable-visible">' + n.orderId+ '</td>' +
							'<td class="footable-visible">' + n.borrowMoney + '</td>' +
							
							'<td class="footable-visible">' + limitPayTime + '</td>' +
							'<td class="footable-visible">' + realPayTime + '</td>' +
							
							'<td class="footable-visible">' + n.overdueDays + '</td>' +
							'<td class="footable-visible">' + n.needPayMoney + '</td>' +
							'<td class="footable-visible">' + n.realPayMoney + '</td>' +
							'<td class="footable-visible">' + n.overdueDays + '</td>' +
							'<td class="footable-visible">' + n.overdueMoney + '</td>' +
							'<td class="footable-visible">' + n.allowDays + '</td>' +
							'<td class="footable-visible">' + n.allowMoney + '</td>' +
							'<td class="footable-visible">' + status + '</td>' +
							'<td class="footable-visible footable-last-column">'+
								'<a  name="已还款" class="btn btn-primary btn-xs" href="javascript:;"  onclick="payMoneyfenqi(' + n.id + ')">已还款</a>&nbsp;'+
								'<a  name="豁免滞纳金" class="btn btn-primary btn-xs" href="javascript:;" data-toggle="modal" data-target="#reduceInterest" onclick="reduceInterestModal(' + id + ')">豁免滞纳金</a></td>'+
								
							'</tr>';
					
						$('#thislist').append(thislist);

					});
					

					if(data.code == 'OVERTIME') {
						var thisUrl = window.location.href;

						if(thisUrl.indexOf('login.html') <= -1) {
							top.window.location.href = "login.html";
						}

					} else {
						if(data.msg != '空数据') {
							//alert(data.msg)
						} else {
							$('#thiscount').text(0);
						}
					}
				}

			},
			error: function() {
				alert("error");
			}
		});
	}
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}



function payMoneyfenqi(id){
	if(confirm("您确定已付款吗？")) {
		$.ajax({
			url: urlcore + "/api/loanOrder/payMoneyFenqi?id=" + id,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				alert(data.msg);
				location.reload()
			},
			error: function() {
				/* Act on the event */
				console.log(data.msg);
				alert(data.msg);
			}
		});
	}

}
function reduceInterestMoney(){
	var money = $("#reduceInterestMoney").val();
	$.ajax({
			url: urlcore + "/api/fenqiPlan/reduceInterestMoney?id=" + fenqiId+"&money="+money,
			type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					alert("修改成功");
					location.reload();
				}else{
					alert(data.msg);
				}
				
			},
			error: function() {
				/* Act on the event */
				console.log(data.msg);
				alert(data.msg);
			}
		});
}
var fenqiId = null;
function reduceInterestModal(id){
	fenqiId = id;
}
