var phone="";
var status = -1;
//var jName = getCookie('Jname');
//我的权限数组
var arrayTitle = new Array; 
$(function(){
	searchuser();
});


function loadMyEssay() {		
	$(document).ready(function() {
//		findMyCatalogue()
		//设置默认第1页
	    init(1);
	});
	
	//默认加载  
	function init(pageNo){
		//获取用户信息列表
		$("#thislist").html(""); 
		$.ajax({
			url: urlcore + "/api/channel/selectList?&pageNo="+pageNo+"&pageSize=10&loginName="+phone,
			type: "get",
			acsny:false,
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success:function(data){
				if (data.success == true) {
					//i表示在data中的索引位置，n表示包含的信息的对象
					$.each(data.data.list,function(i,n){
						var thislist = '<tr>'+
'                                    <td class="footable-visible">'+n.id+'</td>'+
'                                    <td class="footable-visible">'+n.name+'</td>'+
'                                    <td class="footable-visible">'+n.loginName+'</td>'+
'                                    <td class="footable-visible">'+n.linkUrl+'</td>'+
'                                    <td class="footable-visible">'+n.proportion+'</td>'+
'                                    <td class="footable-visible footable-last-column">'+
'                                        <a name="修改" class="btn btn-primary btn-xs" href="channel_add.html?id='+n.id+'">修改</a>'+
'                                        <a name="删除" class="btn btn-primary btn-xs" onclick="detedeOne('+n.id+')">删除</a>'+
'                                        <a name="会员详情" class="btn btn-primary btn-xs" href="channel_member_list.html?channelId='+n.id+'">会员详情</a>'+
'                                        <a name="订单详情" class="btn btn-primary btn-xs" href="channel_orderList.html?channelId='+n.id+'">订单详情</a>'+ 
'                                        <a name="渠道商资料" class="btn btn-primary btn-xs" href="channel_base.html?channelId='+n.id+'">渠道商资料</a>'+ 
'                                    </td>'+
'                                </tr>';
						$('#thislist').append(thislist);
					});
					$.each(arrayTitle, function(i,k) {
						$('a[name="'+k+'"]').attr("hidden",false).attr("class","btn btn-primary btn-xs");
					});
					$('#thiscount').text(data.data.total);
					$("#pager").pager({
						pagenumber: pageNo, 
						pagecount:data.data.pages,
						totalcount:data.data.total,
						buttonClickCallback: PageClick
					}); 
					
				} else if (data.code == 'OVERTIME'){
					var thisUrl = window.location.href;
					if (thisUrl.indexOf('channelLogin.html') <= -1) {
						top.window.location.href="channelLogin.html";
					}

				} else {
					if (data.msg != '空数据') {
						alert(data.msg)
					}else{
						$('#thiscount').text(0);
					}
				}

			},
		});
	}

	//回调函数  
	PageClick = function(pageclickednumber) {  
	    init(pageclickednumber); 
	}

}

//搜索
function searchuser() {
	phone = $('#phone').val().trim();
	status = $('#status').val();
	loadMyEssay();
}



function findMyCatalogue(){
	$.ajax({
		url: urlcore + "/api/roleThirdCatalogue/findAllByUser?secondTitle="+jName,
		type: "GET",
		dataType: 'json',
		async: false,
		contentType: "application/json;charset=utf-8",
		success:function(data){
		if (data.success == true) {
			$.each(data.data, function(i,n) {
				arrayTitle.push(n.thirdCatalogue.title);
			});
		} else {
			alert(data.msg);
		}

		},
		error:function() {
			alert("error");
		}
	});
}


function detedeOne(id){
	if(!confirm("确定删除？")) {
		return;
	}
	$.ajax({
        url:  urlcore + "/api/channel/deleteOne?id="+id,
       		type: "get",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success:function(data){
                if (data.success == true) {
                	loadMyEssay();
                } else if (data.code == 'OVERTIME'){
                    var thisUrl = window.location.href;
                    if (thisUrl.indexOf('channelLogin.html') <= -1) {
                        top.window.location.href="channelLogin.html";
                    }
                } else {
                    alert(data.msg);
                }

        },
        error: function() {
            alert("error");
        }

    });
	
	
	
}
