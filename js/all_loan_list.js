var gmtDatetime = '';
var name = '';
var orderStatus = '';
var phoneNumber = '';
var currentPage = 1;
var totalMoney = 0;
var totalPeople = 0;
var jName = getCookie('Jname');
//我的权限数组
var arrayTitle = new Array; 
loadMyEssay(gmtDatetime, name, phoneNumber, [3,4,5,6,8]);

function loadMyEssay(gmtDatetime, name, phoneNumber, orderStatus) {

	$(document).ready(function() {
		findMyCatalogue();
		init(currentPage);
	});

	function init(pageNo) {
		$("#thislist").html("");
		
		$.ajax({
			url: urlcore + "/api/loanOrder/selectLoanOrderList?gmtDatetime=" + gmtDatetime + "&name=" + name + "&phoneNumber=" + phoneNumber + "&current=" + pageNo+ "&status="+orderStatus,
			type: "get",
			async: 'false',
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				if(data.success == true) {
					$.each(data.data.list, function(i, n) {

						var id = n.id;
						var status = '';
						if(n.orderStatus == "3") {
							status = "待还款"
						} else if(n.orderStatus == "4") {
							status = "容限期中";
						} else if(n.orderStatus == "5") {
							status = "超出容限期中";
						} else if(n.orderStatus == "6") {
							status = "已还款";
						} else if(n.orderStatus == "8") {
							status = "坏账";
						}
						var addr = "";
						if(n.userBasicMsg !=null){
							if(n.userBasicMsg.province != null){
								addr = addr + n.userBasicMsg.province;
							}
							if(n.userBasicMsg.city != null){
								addr = addr + n.userBasicMsg.city;
							}
							if(n.userBasicMsg.county != null){
								addr = addr + n.userBasicMsg.county;
							}
						}
						var thislist =
							'<tr class="footable-even" style="display: table-row;">' +

							'<td class="footable-visible">' + n.id + '</td>' +
							'<td class="footable-visible">' + n.orderNumber + '</td>' +
							'<td class="footable-visible">' + n.user.userName + '</td>' +
							'<td class="footable-visible">' + n.user.phone + '</td>' +
							'<td class="footable-visible">' + n.limitDays/30 + '</td>' +
							'<td class="footable-visible">' + n.borrowMoney + '</td>' +
							'<td class="footable-visible">' + n.realMoney + '</td>' +
							'<td class="footable-visible">' + n.needPayMoney + '</td>' +
							'<td class="footable-visible">' + n.giveTime + '</td>' +
//							'<td class="footable-visible">' + n.limitPayTime + '</td>' +
							'<td class="footable-visible">' + status + '</td>' +
							'<td class="footable-visible">' + addr + '</td>' +
							'<td class="footable-visible">' + n.user.channelName + '</td>' +
							'<td class="footable-visible">' + n.user.referee + '</td>' +
							'<td class="footable-visible footable-last-column">'+
							'<a hidden="hidden" class="" name="分期" href="fenqi_plan_list.html?orderId='+n.id+'" >分期计划</a>&nbsp;'+
								'<a hidden="hidden" class="" name="查看认证信息" href="tab.html?orderId='+n.id+'&id='+n.userId+'&userName='+escape(n.user.userName)+'&phone='+n.user.phone+'"  >查看认证信息</a>&nbsp;'+
								'<a hidden="hidden" name="贷前报告" class="btn btn-primary btn-xs" data='+n.id+' onclick="loansReport(this)"  >贷前报告</a>&nbsp;' +
								'<a hidden="hidden" class="" name="详情" href="all_loan_list_detail.html?id=' + n.id + '" >详情</a>'+
								'<a hidden="hidden" name="合同" class="" href="javascript:;"  onclick="lookContract(\'' + n.agreementUrl + '\')">合同</a></td>'+
							'</tr>';
					
						$('#thislist').append(thislist);

					});
					$.each(arrayTitle, function(i,k) {
						$('a[name="'+k+'"]').attr("hidden",false).attr("class","btn btn-primary btn-xs");
					});
					$("#pager").pager({
						pagenumber: pageNo,
						pagecount: data.data.pages,
						totalcount: data.data.total,
						buttonClickCallback: PageClick
					});

					if(data.code == 'OVERTIME') {
						var thisUrl = window.location.href;

						if(thisUrl.indexOf('login.html') <= -1) {
							top.window.location.href = "login.html";
						}

					} else {
						if(data.msg != '空数据') {
							//alert(data.msg)
						} else {
							$('#thiscount').text(0);
						}
					}
				}

			},
			error: function() {
				alert("error");
			}
		});
	}
	PageClick = function(pageclickednumber) {
		init(pageclickednumber);
	}

}
function lookContract(url){
	window.open(url); 
}
function EnterSearchList() {
	var code = event.keyCode;
	if(code == 13) {
		searchList();
	}

}
function loansReport(obj){
	var orderNo = $(obj).attr('data');
	$.ajax({
		url: urlcore + "/api/loanOrder/selectTdDataByOrderId?orderId=" + orderNo,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		async: false,
		success: function(data) {
			var content = '[' + data+ ']';
			$.showReport(JSON.parse(content));

		},
		error: function() {
			/* Act on the event */
			alert("error");
		}
	});
}
function searchList() {

	var phoneNumber = $('#phoneNumber').val().trim();
	var userName = $('#userName').val().trim();
	var time = $('#applyTime').val().trim();
	loadMyEssay(time, userName, phoneNumber, [3,4,5,6,8]);

}

function checkAuthDetails(id) {

}

function selectOrdersStatus(orderStatus) {
	if(!orderStatus && typeof(orderStatus)!="undefined"){
		loadMyEssay('', '', '', [3,4,5,6,8]);
	}else{
		loadMyEssay('', '', '', [orderStatus]);
	}
	
}

function countPeopleMoney(gmtDatetime, name, phoneNumber, orderStatus) {
	$.ajax({
		url: urlcore + "/api/loanOrder/countPeopleMoney4?gmtDatetime=" + gmtDatetime + "&orderStatus=" + orderStatus + "&name=" + name + "&phoneNumber=" + phoneNumber,
		type: "get",
		dataType: 'json',
		contentType: "application/json;charset=utf-8",
		success: function(data) {
			if(data.success == true) {

				$('#totalMoney').text(data.data.totalMoney);
				$('#totalPeople').text(data.data.totalPeople);

				if(data.code == 'OVERTIME') {
					var thisUrl = window.location.href;

					if(thisUrl.indexOf('login.html') <= -1) {
						top.window.location.href = "login.html";
					}

				} else {
					if(data.msg != '空数据') {
						//alert(data.msg)
					} else {
						$('#thiscount').text(0);
					}
				}
			}

		},
		error: function() {
			alert("error");
		}
	});
}


function findMyCatalogue(){
	$.ajax({
		url: urlcore + "/api/roleThirdCatalogue/findAllByUser?secondTitle="+jName,
		type: "GET",
		dataType: 'json',
		async: false,
		contentType: "application/json;charset=utf-8",
		success:function(data){
		if (data.success == true) {
			$.each(data.data, function(i,n) {
				arrayTitle.push(n.thirdCatalogue.title);
			});
		} else {
			alert(data.msg);
		}

		},
		error:function() {
			alert("error");
		}
	});
}
